from typing import Any, Dict, Iterable, Text
import prettytable


def display(hub, data: Iterable[Dict[str, Any] or Dict]) -> str:
    if not data:
        return ""
    elif isinstance(data, Dict):
        # Make a table for each key in the dict
        ret = []
        for k, v in data.items():
            t: prettytable.PrettyTable = hub.output.table.display(v)
            ret.append(t)
        return "\n".join([str(t) for t in ret if t])
    elif isinstance(data, Text):
        # Assume that this is html
        return "\n".join(str(t) for t in prettytable.from_html(data) if t)
    elif isinstance(data, Iterable):
        # This is the standard use case
        try:
            columns = [
                k for k, v in next(data.__iter__()).items() if isinstance(v, str)
            ]
            table = prettytable.PrettyTable(field_names=columns)

            for row in data:
                if isinstance(row, Dict):
                    table.add_row([row.get(c) for c in columns])
        except AttributeError:
            return ""
    else:
        # Return a string representation of a the unknown data type
        table = hub.output.table.display(str(data))

    return table
