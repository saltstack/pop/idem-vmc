import humanfriendly

__func_alias__ = {"help_": "help"}


async def show(hub, ctx, name: str):
    sddc_id = hub.OPT.vmc.sddc_id or ctx.acct.default_sddc_id
    if name == "t0-routes":
        ret = await hub.tool.vmc.nsxt_proxy.get(
            ctx,
            "policy/api/v1/infra/tier-0s/vmc/routing-table",
            sddc_id=sddc_id,
            enforcement_point_path="/infra/sites/default/enforcement-points/vmc-enforcementpoint",
        )

        return ret[1]["route_entries"]
    elif name == "egress-interface-counters":
        edge_cluster = await hub.tool.vmc.nsxt_proxy.get(
            ctx,
            "policy/api/v1/infra/sites/default/enforcement-points/vmc-enforcementpoint/edge-clusters",
            sddc_id=sddc_id,
        )
        edge_cluster_id = edge_cluster[0]["id"]
        total_bytes = 0
        for edge_id in edge_cluster:
            edge_nodes = await hub.tool.vmc.nsxt_proxy.get(
                ctx,
                f"policy/api/v1/infra/default/enforcement-points/vmc-enforcementpoint/edge-clusters/{edge_cluster_id}/edge-nodes",
                sddc_id=sddc_id,
            )
            edge_path = edge_nodes[edge_id]["path"]
            edge_stat = hub.tool.vmc.nsxt_proxy.get(
                ctx,
                f"policy/api/v1/infra/tier-0s/vmc/locale-services/default/interfaces/public-0/statistics",
                sddc_id=sddc_id,
                edge_path=edge_path,
                enforcement_point_path="policy/api/v1/infra/sites/default/enforcement-points/vmc-enforcementpoint",
            )
            total_bytes += edge_stat["per_node_statistics"][0]["tx"]["total_bytes"]
        return (
            f"Current Total Bytes count on Internet interface is {total_bytes} Bytes."
        )
    elif name == "dns-zones":
        ret = await hub.tool.vmc.nsxt_proxy.get(
            ctx, "policy/api/v1/infra/dns-forwarder-zones", sddc_id=sddc_id,
        )
        return ret
    elif name == "sddc-old-hosts":
        ret = await hub.tool.vmc.org.get(ctx, f"sddcs/{sddc_id}")

        # get the vC block (this is a bad hack to get the rest of the host name
        # shown in vC inventory)
        cdcID = ret["resource_config"]["vc_ip"]
        cdcID = cdcID.split("vcenter")
        cdcID = cdcID[1]
        cdcID = cdcID.split("/")
        cdcID = cdcID[0]

        result = []
        for cluster in ret["resource_config"]["clusters"]:
            for i in cluster["esx_host_list"]:
                name = i["name"]
                result.append(f"{name}{cdcID}")
        return result
    elif name == "sddcs":
        return await hub.exec.vmc.sddc.list(ctx)
    elif name == "org-users":
        return await hub.exec.vmc.org.user.list(ctx)
    elif name == "vms":
        return await hub.tool.vmc.nsxt_proxy.get(
            ctx,
            "policy/api/v1/infra/realized-state/enforcement-points/vmc-enforcementpoint/virtual-machines",
            sddc_id=sddc_id,
        )
    elif name == "connected-accounts":
        return await hub.exec.vmc.account_link.connected_account.list(ctx)
    elif name == "compatible-subnets":
        if not hub.OPT.vmc.region and not hub.OPT.vmc.id:
            raise OSError(
                "Usage: vmctool show compatible-subnets --id=[linkedAccountId] --region=[region]"
            )

        return await hub.exec.vmc.account_link.compatible_subnet.list(
            ctx, region=hub.OPT.vmc.region, account_id=hub.OPT.vmc.id
        )
    elif name == "access-token":
        return ctx.acct.token.access_token
    elif name == "vpn":
        ret = await hub.tool.vmc.nsxt_proxy.get(
            ctx,
            "policy/api/v1/infra/tier-0s/vmc/locale-services/default/ipsec-vpn-services/default/sessions",
            sddc_id=sddc_id,
        )
        result = []
        for vpn in ret:
            result.extend(vpn["rules"])

        return result
    elif name == "vpn-detailed":
        if not hub.OPT.vmc.id:
            raise OSError("Usage: vmctool show vpn-detailed --id=[VPN ID]")

        VPN_ID = hub.OPT.vmc.id
        return await hub.tool.vmc.nsxt_proxy.get(
            ctx,
            f"policy/api/v1/infra/tier-0s/vmc/locale-services/default/ipsec-vpn-services/default/sessions/{VPN_ID}",
            sddc_id=sddc_id,
        )
    elif name == "vpn-stats":
        if not hub.OPT.vmc.id:
            raise OSError("Usage: vmctool show vpn-stats --id=[TUNNEL ID]")
        tunnelID = hub.OPT.vmc.id
        return await hub.tool.vmc.nsxt_proxy.get(
            ctx,
            f"policy/api/v1/infra/tier-0s/vmc/locale-services/default/ipsec-vpn-services/default/sessions/{tunnelID}/statistics",
            sddc_id=sddc_id,
        )
    elif name == "vpn-ike-profile":
        return await hub.tool.vmc.nsxt_proxy.get(
            ctx, "policy/api/v1/infra/ipsec-vpn-ike-profiles", sddc_id=sddc_id,
        )
    elif name == "l2vpn-services":
        return await hub.tool.vmc.nsxt_proxy.get(
            ctx,
            "policy/api/v1/infra/tier-0s/vmc/locale-services/default/l2vpn-services/default",
            sddc_id=sddc_id,
        )
    elif name == "l2vpn":
        return await hub.tool.vmc.nsxt_proxy.get(
            ctx,
            "policy/api/v1/infra/tier-0s/vmc/locale-services/default/l2vpn-services/default/sessions",
            sddc_id=sddc_id,
        )
    elif name == "vpn-ipsec-tunnel-profile":
        return await hub.tool.vmc.nsxt_proxy.get(
            ctx, "policy/api/v1/infra/ipsec-vpn-tunnel-profiles", sddc_id=sddc_id,
        )
    elif name == "vpn-ipsec-endpoints":
        return await hub.tool.vmc.nsxt_proxy.get(
            ctx, "policy/api/v1/infra/ipsec-vpn-tunnel-profiles", sddc_id=sddc_id,
        )
    elif name == "network":
        return await hub.tool.vmc.nsxt_proxy.get(
            ctx, "policy/api/v1/infra/tier-1s/cgw/segments", sddc_id=sddc_id,
        )
    elif name == "nat":
        if hub.OPT.vmc.id:
            NATid = hub.OPT.vmc.id
            return await hub.tool.vmc.nsxt_proxy.get(
                ctx,
                f"policy/api/v1/infra/tier-1s/cgw/nat/USER/nat-rules/{NATid}/statistics",
                sddc_id=sddc_id,
            )
        else:
            return await hub.tool.vmc.nsxt_proxy.get(
                ctx,
                f"policy/api/v1/infra/tier-1s/cgw/nat/USER/nat-rules",
                sddc_id=sddc_id,
            )
    elif name == "cgw-rule":
        return await hub.tool.vmc.nsxt_proxy.get(
            ctx,
            f"policy/api/v1/infra/domains/cgw/gateway-policies/default/rules",
            sddc_id=sddc_id,
        )
    elif name == "mgw-rule":
        return await hub.tool.vmc.nsxt_proxy.get(
            ctx,
            f"policy/api/v1/infra/domains/mgw/gateway-policies/default/rules",
            sddc_id=sddc_id,
        )
    elif name == "dfw-section-rules":
        if not hub.OPT.vmc.id:
            raise OSError(
                "Usage: vmctool show dfw-section-rules --section=[SECTION NAME]"
            )
        section = hub.OPT.vmc.section
        return await hub.tool.vmc.nsxt_proxy.get(
            ctx,
            f"policy/api/v1/infra/domains/cgw/security-policies/{section}/rules",
            sddc_id=sddc_id,
        )
    elif name == "dfw-section":
        return await hub.tool.vmc.nsxt_proxy.get(
            ctx, f"policy/api/v1/infra/domains/cgw/security-policies", sddc_id=sddc_id,
        )
    elif name == "mtu":
        ret = await hub.tool.vmc.nsxt_proxy.get(
            ctx, f"cloud-service/api/v1/infra/external/config", sddc_id=sddc_id,
        )
        size = humanfriendly.format_size(ret["intranet_mtu"])
        return f"The MTU over the Direct Connect is {size}."
    elif name == "shadow-account":
        ret = await hub.tool.vmc.nsxt_proxy.get(
            ctx, f"cloud-service/api/v1/infra/accounts", sddc_id=sddc_id,
        )
        return humanfriendly.format_size(int(ret["shadow_account"]))
    elif name == "sddc-old-bgp-as":
        ret = await hub.tool.vmc.nsxt_proxy.get(
            ctx, f"cloud-service/api/v1/infra/direct-connect/bgp", sddc_id=sddc_id,
        )
        return ret["local_as_num"]
    elif name == "sddc-old-bgp-vpn":
        ret = await hub.tool.vmc.nsxt_proxy.get(
            ctx, f"cloud-service/api/v1/infra/direct-connect/bgp", sddc_id=sddc_id,
        )
        SDDC_BGP_VPN = ret["route_preference"]
        if SDDC_BGP_VPN == "VPN_PREFERRED_OVER_DIRECT_CONNECT":
            return "The preferred path is over VPN, with Direct Connect as a back-up."
        else:
            return "The preferred path is over Direct Connect, with VPN as a back-up."
    elif name == "sddc-old-connected-vpc":
        ret = await hub.tool.vmc.nsxt_proxy.get(
            ctx, f"cloud-service/api/v1/infra/linked-vpcs", sddc_id=sddc_id,
        )
        result = []
        for connected_vpc in ret:
            services = await hub.tool.vmc.nsxt_proxy.get(
                ctx,
                f"cloud-service/api/v1/infra/linked-vpcs/{connected_vpc['linked_vpc_id']}/connected-services",
                sddc_id=sddc_id,
            )
            connected_vpc["Service Access"] = ", ".join([x["name"] for x in services])
            result.append(connected_vpc)
        return ret
    elif name == "sddc-old-public-ip":
        return await hub.tool.vmc.nsxt_proxy.get(
            ctx, f"cloud-service/api/v1/infra/public-ips", sddc_id=sddc_id,
        )
    elif name == "vpn-internet-ip":
        ret = await hub.tool.vmc.nsxt_proxy.get(
            ctx, f"cloud-service/api/v1/infra/sddc-user-config", sddc_id=sddc_id,
        )
        return {"VPN Internet IPs": ret["vpn_internet_ips"]}
    elif name == "sddc-old-state":
        return await hub.exec.vmc.sddc.get(ctx, sddc_id)
    elif name == "group":
        if hub.OPT.vmc.id and hub.OPT.vmc.gateway:
            group_id = hub.OPT.vmc.id
            gw = hub.OPT.vmc.gateway
            return await hub.tool.vmc.nsxt_proxy.get(
                ctx,
                f"policy/api/v1/infra/domains/{gw}/groups/{group_id}",
                sddc_id=sddc_id,
            )
        elif hub.OPT.vmc.gateway:
            gw = hub.OPT.vmc.gateway
            return await hub.tool.vmc.nsxt_proxy.get(
                ctx, f"policy/api/v1/infra/domains/{gw}/groups", sddc_id=sddc_id,
            )
        else:
            return {
                "management_groups": await hub.tool.vmc.nsxt_proxy.get(
                    ctx, f"policy/api/v1/infra/domains/mgw/groups", sddc_id=sddc_id,
                ),
                "compute_groups": await hub.tool.vmc.nsxt_proxy.get(
                    ctx, f"policy/api/v1/infra/domains/cgw/groups", sddc_id=sddc_id,
                ),
            }
    elif name == "dns-services":
        if hub.OPT.vmc.gateway:
            gw = hub.OPT.vmc.gateway
            return await hub.tool.vmc.nsxt_proxy.get(
                ctx, f"policy/api/v1/infra/tier-1s/{gw}/dns-forwarder", sddc_id=sddc_id,
            )
        else:
            return {
                "management_dns_services": await hub.tool.vmc.nsxt_proxy.get(
                    ctx,
                    f"policy/api/v1/infra/tier-1s/mgw/dns-forwarder",
                    sddc_id=sddc_id,
                ),
                "compute_dns_services": await hub.tool.vmc.nsxt_proxy.get(
                    ctx,
                    f"policy/api/v1/infra/tier-1s/cgw/dns-forwarder",
                    sddc_id=sddc_id,
                ),
            }
    elif name == "service":
        if hub.OPT.vmc.id:
            service_id = hub.OPT.vmc.id
            return await hub.tool.vmc.nsxt_proxy.get(
                ctx, f"policy/api/v1/infra/services/{service_id}", sddc_id=sddc_id,
            )
        else:
            return await hub.tool.vmc.nsxt_proxy.get(
                ctx, f"policy/api/v1/infra/services", sddc_id=sddc_id,
            )
    elif name == "services":
        return await hub.tool.vmc.nsxt_proxy.get(
            ctx, f"policy/api/v1/infra/services", sddc_id=sddc_id,
        )
    elif name == "token":
        return ctx.acct.token
    elif name == "profile":
        return ctx.acct.profile
    elif name == "acct":
        return ctx.acct
    else:
        hub.vmc.client.help()


async def intent(hub, ctx, name: str):
    raise NotImplementedError(
        "Intents like this should be handled in exec modules and states"
    )
    if name == "create-lots-networks":
        raise NotImplementedError()
        number = hub.OPT.vmc.number
        return createLotsNetworks(proxy, session_token, number)
    elif name == "new-vpn":
        raise NotImplementedError()
        vpn_name = input("Enter the VPN Name: ")
        remote_private_ip = input("Enter the remote private IP:")
        remote_public_ip = input("Enter the remote public IP:")
        source_networks = input(
            "Enter your source networks, separated by commas (for example: 192.168.10.0/24,192.168.20.0/24)"
        )
        destination_networks = input(
            "Enter your destination networks, separated by commas (for example: 192.168.10.0/24,192.168.20.0/24)"
        )
    elif name == "new-l2vpn":
        if not hub.OPT.vmc.name and not hub.OPT.vmc.endpoint and not hub.OPT.vmc.ip:
            raise OSError(
                "Usage: vmctool client new-l2vpn --ip=[peer ip] --endpoint=[endpoint], --name=[display name]"
            )
        display_name = hub.OPT.vmc.name
        endpoint = hub.OPT.vmc.endpoint
        peer_ip = hub.OPT.vmc.ip
        hub.log.info("Creating an IPSec VPN IKE Profile...")
        raise NotImplementedError()
        ike_profile = newSDDCIPSecVpnIkeProfile(proxy, session_token, display_name)
        hub.log.info(ike_profile)
        hub.log.info("Creating an IPSec VPN Tunnel Profile...")
        tunnel_profile = newSDDCIPSecVpnTunnelProfile(
            proxy, session_token, display_name
        )
        hub.log.info(tunnel_profile)
        hub.log.info("Creating an IPSec VPN Session...")
        vpn_session = newSDDCIPSecVpnSession(
            proxy, session_token, display_name, endpoint, peer_ip
        )
        hub.log.info(vpn_session)
        hub.log.info("Creating an L2 VPN Session...")
        return newSDDCL2VPN(proxy, session_token, display_name)
    elif name == "remove-vpn-ipsec-tunnel-profile":
        if not hub.OPT.vmc.id:
            raise OSError(
                "Usage: vmctool client remove-vpn-ipsec-tunnel-profile --id=[VPN ID]"
            )

        raise NotImplementedError()
        return removeSDDCIPSecVpnTunnelProfile(proxy, session_token, hub.OPT.vmc.id)
    elif name == "remove-vpn-ike-profile":
        if not hub.OPT.vmc.id:
            raise OSError("Usage: vmctool client remove-vpn-ike-profile --id=[VPN ID]")
        raise NotImplementedError()
        return removeSDDCIPSecVpnIkeProfile(proxy, session_token, hub.OPT.vmc.id)
    elif name == "remove-vpn":
        if not hub.OPT.vmc.id:
            raise OSError("Usage: vmctool client remove-vpn --id=[VPN ID]")
        raise NotImplementedError()
        return removeSDDCVPN(proxy, session_token, hub.OPT.vmc.id)
    elif name == "remove-l2vpn":
        if not hub.OPT.vmc.id:
            raise OSError("Usage: vmctool client remove-l2vpn --id=[VPN ID]")
        raise NotImplementedError()
        return removeSDDCL2VPN(proxy, session_token, hub.OPT.vmc.id)
    elif name == "new-network":
        if not hub.OPT.vmc.name:
            raise OSError(
                "Usage: vmctool client new-network --connection-type=[CONNECTION TYPE] --name=[display_name]"
            )
        if hub.OPT.vmc.connection_type == "routed":
            # DHCP-Enabled Network
            if not hub.OPT.vmc.gateway:
                raise OSError(
                    "Usage: vmctool client new-network --connection-type=routed --name=[display_name] --gateway=[gateway]"
                )
            display_name = hub.OPT.vmc.name
            routing_type = "ROUTED"
            gateway_address = hub.OPT.vmc.gateway
            dhcp_range = hub.OPT.vmc.dhcp_range
            domain_name = hub.OPT.vmc.domain
            raise NotImplementedError()
            return newSDDCnetworks(
                proxy,
                session_token,
                display_name,
                gateway_address,
                dhcp_range,
                domain_name,
                routing_type,
            )
        elif hub.OPT.vmc.connection_type == "disconnected":
            #  Disconnected Network
            if not hub.OPT.vmc.gateway:
                raise OSError(
                    "Usage: vmctool client new-network --connection-type=disconnected --name=[display_name] --gateway=[gateway]"
                )
            display_name = hub.OPT.vmc.name
            routing_type = "DISCONNECTED"
            gateway_address = hub.OPT.vmc.gateway
            dhcp_range = ""
            domain_name = ""
            raise NotImplementedError()
            newSDDC = newSDDCnetworks(
                proxy,
                session_token,
                display_name,
                gateway_address,
                dhcp_range,
                domain_name,
                routing_type,
            )
            hub.log.info(newSDDC)
        elif hub.OPT.vmc.connection_type == "extended":
            if not hub.OPT.vmc.gateway:
                raise OSError(
                    "Usage: vmctool client new-network --connection-type=disconnected --name=[display_name] --gateway=[gateway] --id=[TUNNEL ID]"
                )
            display_name = hub.OPT.vmc.name
            tunnel_id = hub.OPT.vmc.id
            raise NotImplementedError()
            l2vpn_path = getSDDCL2VPNSessionPath(proxy, session_token)
            return newSDDCStretchednetworks(
                proxy, session_token, display_name, tunnel_id, l2vpn_path
            )
        else:
            hub.vmc.client.help()
    elif name == "remove-network":
        if not hub.OPT.vmc.id:
            raise OSError("Usage: vmctool client remove-network --id=[NETWORK ID]")
        raise NotImplementedError()
        return removeSDDCNetworks(proxy, session_token, hub.OPT.vmc.id)
    elif name == "new-nat-rule":
        if not hub.OPT.vmc.name:
            raise OSError(
                "Usage: vmctool client new-nat-rule --nat-action=[REFLEXIVE|DNAT] --name=[DISPLAY NAME]"
            )
        raise NotImplementedError()
        display_name = hub.OPT.vmc.name
        action = hub.OPT.vmc.action
        if action == "REFLEXIVE":
            translated_network = hub.OPT.vmc.translated_network
            source_network = hub.OPT.vmc.source_network
            service = ""
            translated_port = ""
            raise NotImplementedError()
            return newSDDCNAT(
                proxy,
                session_token,
                display_name,
                action,
                translated_network,
                source_network,
                service,
                translated_port,
                logging,
                status,
            )
        elif action == "DNAT":
            translated_network = hub.OPT.vmc.translated_network
            source_network = hub.OPT.vmc.translated_network
            service = hub.OPT.vmc.service
            translated_port = hub.OPT.vmc.translated_port
            raise NotImplementedError()
            hub.log.info(
                newSDDCNAT(
                    proxy,
                    session_token,
                    display_name,
                    action,
                    translated_network,
                    source_network,
                    service,
                    translated_port,
                    logging,
                    status,
                )
            )
    elif name == "remove-nat-rule":
        if not hub.OPT.vmc.id:
            raise OSError("Usage: vmctool client remove-nat-rule --id=[VPN ID]")
        raise NotImplementedError()
        result = removeSDDCNAT(proxy, session_token, hub.OPT.vmc.id)
        hub.log.info(result)
        return getSDDCNAT(proxy, session_token)
    elif name == "new-dfw-rule":
        sequence_number = 0
        display_name = hub.OPT.vmc.name
        sg_string = hub.OPT.vmc.source_groups
        dg_string = hub.OPT.vmc.dest_groups
        group_index = "/infra/domains/cgw/groups/"
        scope_index = "/infra/labels/cgw-"
        list_index = "/infra/services/"
        if sg_string.lower() == "connected_vpc":
            source_groups = ["/infra/tier-0s/vmc/groups/connected_vpc"]
        elif sg_string.lower() == "directconnect_prefixes":
            source_groups = ["/infra/tier-0s/vmc/groups/directConnect_prefixes"]
        elif sg_string.lower() == "s3_prefixes":
            source_groups = ["/infra/tier-0s/vmc/groups/s3_prefixes"]
        elif sg_string.lower() == "any":
            source_groups = ["ANY"]
        else:
            sg_list = sg_string.split(",")
            source_groups = [group_index + x for x in sg_list]
        if dg_string.lower() == "connected_vpc":
            destination_groups = ["/infra/tier-0s/vmc/groups/connected_vpc"]
        elif dg_string.lower() == "directconnect_prefixes":
            destination_groups = ["/infra/tier-0s/vmc/groups/directConnect_prefixes"]
        elif dg_string.lower() == "s3_prefixes":
            destination_groups = ["/infra/tier-0s/vmc/groups/s3_prefixes"]
        elif dg_string.lower() == "any":
            destination_groups = ["ANY"]
        else:
            dg_list = dg_string.split(",")
            destination_groups = [group_index + x for x in dg_list]
        services_string = hub.OPT.vmc.service
        if services_string.lower() == "any":
            services = ["ANY"]
        else:
            services_list = services_string.split(",")
            services = [list_index + x for x in services_list]
        action = hub.OPT.vmc.action
        section = hub.OPT.vmc.section

        if hub.OPT.vmc.number:
            sequence_number = hub.OPT.vmc.number
            raise NotImplementedError()
            new_rule = newSDDCDFWRule(
                proxy,
                session_token,
                display_name,
                source_groups,
                destination_groups,
                services,
                action,
                section,
                sequence_number,
            )
        else:
            raise NotImplementedError()
            new_rule = newSDDCDFWRule(
                proxy,
                session_token,
                display_name,
                source_groups,
                destination_groups,
                services,
                action,
                section,
                sequence_number,
            )
        if new_rule == 200:
            hub.log.info("\n The rule has been created.")
            return getSDDCDFWRule(proxy, session_token, section)
    elif name == "remove-dfw-rule":
        if not hub.OPT.vmc.id and not hub.OPT.vmc.section:
            raise OSError(
                "Usage: vmctool client remove-dfw-rule --id=[RULE ID] --section=[SECTION ID]"
            )
        section_id = hub.OPT.vmc.section
        rule_id = hub.OPT.vmc.id
        raise NotImplementedError()
        if removeSDDCDFWRule(proxy, session_token, section_id, rule_id) == 200:
            hub.log.info("The rule " + rule_id + " has been deleted")
            return getSDDCDFWRule(proxy, session_token, section_id)
        else:
            hub.log.info("Issues deleting the security rule. Check the syntax.")
    elif name == "new-dfw-section":
        if not hub.OPT.vmc.name and hub.OPT.vmc.category:
            raise OSError(
                "Usage: vmctool client new-dfw-section --name=[NAME] --category=[DFW SECTION CATEGORY]"
            )
        name = hub.OPT.vmc.name
        category = hub.OPT.vmc.category
        raise NotImplementedError()
        status_code = newSDDCDFWSection(proxy, session_token, name, category)
        if status_code == 200:
            hub.log.info("Success:")
            hub.log.info(
                "\nThe section "
                + name
                + " has been created in the "
                + category
                + " category."
            )
            return getSDDCDFWSection(proxy, session_token)
    elif name == "remove-dfw-section":
        section_id = hub.OPT.vmc.section
        raise NotImplementedError()
        if removeSDDCDFWSection(proxy, session_token, section_id) == 200:
            hub.log.info(f"The section {section_id} has been deleted.")
            return getSDDCDFWSection(proxy, session_token)
    elif name == "set-mtu":
        if not hub.OPT.vmc.number:
            raise OSError("Usage: vmctool client set-mtu --number=[MTU]")
        mtu = hub.OPT.vmc.number
        if int(mtu) < 1500 or int(mtu) > 8900:
            hub.log.info(
                "Incorrect syntax. The MTU should be between 1500 and 8900 bytes."
            )
        else:
            raise NotImplementedError()
            setMTU = setSDDCMTU(proxy, session_token, mtu)
            if setMTU == 200:
                hub.log.info("The MTU has been updated:")
                hub.log.info(
                    f"The MTU over the Direct Connect is now set to {getSDDCMTU(proxy, session_token)} Bytes."
                )
    elif name == "set-sddc_old-bgp-as":
        if not hub.OPT.vmc.number:
            raise OSError("Usage: vmctool client set-sddc_old-bgp-as --number=[ASN]")
        asn = hub.OPT.vmc.number
        raise NotImplementedError()
        setasn = setSDDCBGPAS(proxy, session_token, asn)
        hub.log.info(setasn)
        if setasn == 200:
            hub.log.info("The BGP AS has been updated:")
            return f"The SDDC BGP Autonomous System is ASN {getSDDCBGPAS(proxy, session_token)}."
        else:
            hub.log.info("There was an error. Check the syntax.")
            return f"The SDDC BGP Autonomous System is ASN {getSDDCBGPAS(proxy, session_token)}."
    elif name == "set-sddc_old-connected-services":
        value = str(hub.OPT.vmc.enabled).lower()
        raise NotImplementedError()
        if (
            setSDDCConnectedServices(proxy, session_token, value) == 200
            and value == "true"
        ):
            hub.log.info("S3 access from the SDDC is over the ENI.")
        elif (
            setSDDCConnectedServices(proxy, session_token, value) == 200
            and value == "false"
        ):
            hub.log.info("S3 access from the SDDC is over the Internet.")
    elif name == "new-sddc_old-public-ip":
        if not hub.OPT.vmc.notes:
            raise OSError(
                "Usage: vmctool client new-sddc_old-public-ip --notes=[NOTES]"
            )
        notes = hub.OPT.vmc.notes
        raise NotImplementedError()
        if newSDDCPublicIP(proxy, session_token, notes) == 200:
            return getSDDCPublicIP(proxy, session_token)
        else:
            raise RuntimeError("Issues creating a Public IP.")
    elif name == "set-public-ip":
        if not hub.OPT.vmc.ip and not hub.OPT.vmc.notes:
            raise OSError(
                "Usage: vmctool client set-public-ip --ip=[PUBLIC IP] --notes=[NOTES]"
            )
        public_ip = hub.OPT.vmc.ip
        notes = hub.OPT.vmc.notes
        raise NotImplementedError()
        if setSDDCPublicIP(proxy, session_token, notes, public_ip) == 200:
            return getSDDCPublicIP(proxy, session_token)
    elif name == "remove-sddc_old-public-ip":
        if not hub.OPT.vmc.ip:
            raise OSError(
                "Usage: vmctool client remove-sddc_old-public-ip --ip=[PUBLIC IP]"
            )
        public_ip = hub.OPT.vmc.ip
        raise NotImplementedError()
        if removeSDDCPublicIP(proxy, session_token, public_ip) == 200:
            return getSDDCPublicIP(proxy, session_token)
    elif name == "new-mgw-rule":
        display_name = hub.OPT.vmc.name
        # String and List Manipulation:
        sg_string = hub.OPT.vmc.source_groups
        dg_string = hub.OPT.vmc.dest_groups
        group_index = "/infra/domains/mgw/groups/"
        list_index = "/infra/services/"
        if sg_string.lower() == "any":
            source_groups = ["ANY"]
        else:
            sg_string = sg_string.upper()
            sg_list = sg_string.split(",")
            source_groups = [group_index + x for x in sg_list]

        # String and List Manipulation:
        # We take the input argument (NSX-MANAGER or VCENTER or ESXI nodes)

        if dg_string.lower() == "any":
            destination_groups = ["ANY"]
        else:
            dg_string = dg_string.upper()
            dg_list = dg_string.split(",")
            destination_groups = [group_index + x for x in dg_list]

        services_string = hub.OPT.vmc.service
        if services_string.lower() == "any":
            services = ["ANY"]
        else:
            services_list = services_string.split(",")
            hub.log.info(services_list)
            services = [list_index + x for x in services_list]
        action = hub.OPT.vmc.action
        raise NotImplementedError()
        if hub.OPT.vmc.number:
            sequence_number = hub.OPT.vmc.number
            new_rule = newSDDCMGWRule(
                proxy,
                session_token,
                display_name,
                source_groups,
                destination_groups,
                services,
                action,
                sequence_number,
            )
            hub.log.info(new_rule)
        else:
            new_rule = newSDDCMGWRule(
                proxy,
                session_token,
                display_name,
                source_groups,
                destination_groups,
                services,
                action,
                0,
            )
        if new_rule == 200:
            hub.log.info("\n The rule has been created.")
            return getSDDCMGWRule(proxy, session_token)
    elif name == "new-cgw-rule":
        raise NotImplementedError(
            "complete rework on this, need to replace sys.argv with hub.OPT"
        )
        display_name = hub.OPT.vmc.name
        sg_string = hub.OPT.vmc.source_groups
        dg_string = hub.OPT.vmc.dest_groups
        group_index = "/infra/domains/cgw/groups/"
        scope_index = "/infra/labels/cgw-"
        list_index = "/infra/services/"
        if sg_string.lower() == "connected_vpc":
            source_groups = ["/infra/tier-0s/vmc/groups/connected_vpc"]
        elif sg_string.lower() == "directconnect_prefixes":
            source_groups = ["/infra/tier-0s/vmc/groups/directConnect_prefixes"]
        elif sg_string.lower() == "s3_prefixes":
            source_groups = ["/infra/tier-0s/vmc/groups/s3_prefixes"]
        elif sg_string.lower() == "any":
            source_groups = ["ANY"]
        else:
            sg_list = sg_string.split(",")
            source_groups = [group_index + x for x in sg_list]
        if dg_string.lower() == "connected_vpc":
            destination_groups = ["/infra/tier-0s/vmc/groups/connected_vpc"]
        elif dg_string.lower() == "directconnect_prefixes":
            destination_groups = ["/infra/tier-0s/vmc/groups/directConnect_prefixes"]
        elif dg_string.lower() == "s3_prefixes":
            destination_groups = ["/infra/tier-0s/vmc/groups/s3_prefixes"]
        elif dg_string.lower() == "any":
            destination_groups = ["ANY"]
        else:
            dg_list = dg_string.split(",")
            destination_groups = [group_index + x for x in dg_list]
        services_string = hub.OPT.vmc.service
        if services_string.lower() == "any":
            services = ["ANY"]
        else:
            services_list = services_string.split(",")
            services = [list_index + x for x in services_list]
        action = hub.OPT.vmc.action
        scope_string = sys.argv[7].lower()
        scope_list = scope_string.split(",")
        scope = [scope_index + x for x in scope_list]
        if hub.OPT.vmc.number:
            sequence_number = hub.OPT.vmc.number
            new_rule = newSDDCCGWRule(
                proxy,
                session_token,
                display_name,
                source_groups,
                destination_groups,
                services,
                action,
                scope,
                sequence_number,
            )
        else:
            new_rule = newSDDCCGWRule(
                proxy,
                session_token,
                display_name,
                source_groups,
                destination_groups,
                services,
                action,
                scope,
                0,
            )
        if new_rule == 200:
            hub.log.info("\n The rule has been created.")
            return getSDDCCGWRule(proxy, session_token)
    elif name == "remove-cgw-rule":
        if not hub.OPT.vmc.id:
            raise OSError("Usage: vmctool client remove-cgw-rule --id=[RULE ID]")
        rule_id = hub.OPT.vmc.id
        raise NotImplementedError()
        if removeSDDCCGWRule(proxy, session_token, rule_id) == 200:
            hub.log.info(f"The rule {rule_id} has been deleted")
            return getSDDCCGWRule(proxy, session_token)
    elif name == "remove-mgw-rule":
        if not hub.OPT.vmc.id:
            raise OSError("Usage: vmctool client remove-mgw-rule --id=[RULE ID]")
        rule_id = hub.OPT.vmc.id
        raise NotImplementedError()
        if removeSDDCMGWRule(proxy, session_token, rule_id) == 200:
            return getSDDCMGWRule(proxy, session_token)
    elif name == "new-group":
        raise NotImplementedError(
            "complete rework on this, need to replace sys.argv with hub.OPT"
        )
        gw = hub.OPT.vmc.gateway
        group_id = hub.OPT.vmc.id
        if gw == "mgw" and hub.OPT.vmc.ip:
            ip_addresses_string = hub.OPT.vmc.ip
            ip_addresses = ip_addresses_string.split(",")
            return newSDDCGroupIPaddress(
                proxy, session_token, gw, group_id, ip_addresses
            )
        elif gw == "mgw":
            ip_addresses = []
            ip_address = ""
            # Start a loop that will run until the user enters 'done'.
            while ip_address != "done":
                # Ask the user for a name.
                ip_address = input(
                    "Please enter IP address (for example, \"172.16.10.20\") or type 'done' when your list is finished:"
                )
                # Add the new name to our list.
                if ip_address != "done":
                    ip_addresses.append(ip_address)
            return newSDDCGroupIPaddress(
                proxy, session_token, gw, group_id, ip_addresses
            )
        if gw == "cgw":
            group_criteria = hub.OPT.vmc.group_criteria
            if group_criteria == "ip-based" and hub.OPT.vmc.ip:
                ip_addresses_string = hub.OPT.vmc.ip
                ip_addresses = ip_addresses_string.split(",")
                return newSDDCGroupIPaddress(
                    proxy, session_token, gw, group_id, ip_addresses
                )
            elif group_criteria == "ip-based":
                ip_addresses = []
                # Set new_name to something other than 'quit'.
                ip_address = ""
                # Start a loop that will run until the user enters 'quit'.
                while ip_address != "done":
                    # Ask the user for a name.
                    ip_address = input(
                        "Please enter IP address (\"172.16.10.20\") or type 'done' when your list is finished: "
                    )
                    # Add the new name to our list.
                    if ip_address != "done":
                        ip_addresses.append(ip_address)
                return newSDDCGroupIPaddress(
                    proxy, session_token, gw, group_id, ip_addresses
                )
            elif group_criteria == "criteria-based":
                # Only support for Virtual_Machine based criteria for now.
                # member_type = input("Please enter your criteria type:")
                member_type = "VirtualMachine"
                key = input(
                    "Please enter the criteria (Name, Tag, OSName or ComputerName): "
                )
                if key not in ["Name", "Tag", "OSName", "ComputerName"]:
                    hub.log.info("Incorrect syntax. Check again.")
                else:
                    operator = input(
                        "Please enter the operator (EQUALS, NOTEQUALS, CONTAINS, STARTSWITH, ENDSWITH): "
                    )
                    if operator not in [
                        "EQUALS",
                        "NOTEQUALS",
                        "CONTAINS",
                        "STARTSWITH",
                        "ENDSWITH",
                    ]:
                        hub.log.info("Incorrect syntax. Check again.")
                    if key == "Tag" and operator == "NOTEQUALS":
                        hub.log.info(
                            "Incorrect syntax. The tag method does not support the NOTEQUALS Operator. Try again."
                        )
                    else:
                        value = input("Enter the value of your membership criteria: ")
                        return newSDDCGroupCriteria(
                            proxy,
                            session_token,
                            gw,
                            group_id,
                            member_type,
                            key,
                            operator,
                            value,
                        )
            elif group_criteria == "criteria-based" and len(sys.argv) == 8:
                # Only support for Virtual_Machine based criteria for now.
                # member_type = input("Please enter your criteria type:")
                member_type = "VirtualMachine"
                key = sys.argv[5]
                operator = sys.argv[6]
                value = sys.argv[7]
                if key not in ["Name", "Tag", "OSName", "ComputerName"]:
                    hub.log.info("Incorrect syntax. Check again.")
                return newSDDCGroupCriteria(
                    proxy,
                    session_token,
                    gw,
                    group_id,
                    member_type,
                    key,
                    operator,
                    value,
                )
            elif group_criteria == "member-based" and len(sys.argv) == 5:
                # v1 will be based on a list of VMs. Will not include segment-based for the time being,
                vm_list = []
                # Set new_name to something other than 'quit'.
                vm_name = ""
                # Start a loop that will run until the user enters 'quit'.
                while vm_name != "done":
                    # Ask the user for a name.
                    vm_name = input(
                        "Please enter the name of the VM or type 'done' when your list is finished: "
                    )
                    # Add the new name to our list.
                    if vm_name != "done":
                        vm_id = getVMExternalID(proxy, session_token, vm_name)
                        vm_list.append(vm_id)
                newSDDCGroup = newSDDCGroupVM(
                    proxy, session_token, gw, group_id, vm_list
                )
                hub.log.info(newSDDCGroup)
            elif group_criteria == "member-based" and len(sys.argv) == 6:
                vm_name_string = sys.argv[5]
                vm_name_list = vm_name_string.split(",")
                ## iterate through list or through previous string to get list of external ids
                vm_external_id_list = [
                    getVMExternalID(proxy, session_token, x) for x in vm_name_list
                ]
                # vm_id = getVMExternalID(proxy,session_token,vm_name)
                newSDDCGroup = newSDDCGroupVM(
                    proxy, session_token, gw, group_id, vm_external_id_list
                )
                hub.log.info(newSDDCGroup)
            elif group_criteria == "group-based":
                # Example: new-group cgw new-group-name group-based existing-group-to-add-as-member
                group_name_string = sys.argv[5]
                retval = newSDDCGroupGr(
                    proxy, session_token, gw, group_id, group_name_string
                )
                if retval == 200:
                    hub.log.info("Group created")
                else:
                    hub.log.info("Could not create group")
    elif name == "remove-group":
        if not hub.OPT.vmc.id and not hub.OPT.vmc.gateway:
            raise OSError(
                "Usage: vmctool client remove-group --id=[GROUP ID] --gateway=[GATEWAY]"
            )

        gw = hub.OPT.vmc.gateway
        group_id = hub.OPT.vmc.id
        raise NotImplementedError()
        return removeSDDCGroup(proxy, session_token, gw, group_id)
    elif name == "new-service":
        if hub.OPT.vmc.name and hub.OPT.vmc.service_entry:
            name = hub.OPT.vmc.name
            service_entry_string = hub.OPT.vmc.service_entry
            service_entry_list = service_entry_string.split(",")
            raise NotImplementedError()
            newSDDCService(proxy, session_token, name, service_entry_list)
            return getSDDCService(proxy, session_token, service_id)
        else:
            service_id = input("Please input the name of the service:")
            service_entry_list = []
            # Start a loop that will run until the user enters 'quit'.
            # Ask the user for a name.
            destination_port = ""
            while destination_port != "done":
                destination_port_list = []
                source_port_list = []
                service_entry_id = input("Please enter the Service Entry ID:")
                l4_protocol = input("Please enter the L4 Protocol:")
                source_port = ""
                destination_port = ""
                while source_port != "done":
                    source_port = input(
                        "Plese enter the Source Ports or type 'done' when your list is finished:"
                    )
                    if source_port != "done":
                        source_port_list.append(source_port)
                while (destination_port != "next") and (destination_port != "done"):
                    source_port = ""
                    destination_port = input(
                        "Plese enter the Destination Ports, type 'next' when you want to define another service entry or 'done' if you have finished:"
                    )
                    if (destination_port != "next") and (destination_port != "done"):
                        destination_port_list.append(destination_port)
                # hub.log.info(service_id)
                #  hub.log.info(destination_port_list)
                #  hub.log.info(source_port_list)
                #  hub.log.info(l4_protocol)
                service_entry = {
                    "l4_protocol": l4_protocol,
                    "source_ports": source_port_list,
                    "destination_ports": destination_port_list,
                    "resource_type": "L4PortSetServiceEntry",
                    "id": service_entry_id,
                    "display_name": service_entry_id,
                }
                service_entry_list.append(service_entry)
                #  hub.log.info(service_entry)
                #  hub.log.info(service_entry_list)
            raise NotImplementedError()
            newSDDCService(proxy, session_token, service_id, service_entry_list)
            return getSDDCService(proxy, session_token, service_id)
    elif name == "new-service-entry":
        raise NotImplementedError("This is WIP")
    elif name == "remove-service":
        if not hub.OPT.vmc.id:
            raise OSError("Usage: vmctool client remove-service --id=[SERVICE ID]")
        service_id = hub.OPT.vmc.id
        raise NotImplementedError()
        sddc_service_delete = removeSDDCService(proxy, session_token, service_id)
    else:
        hub.vmc.client.help()


def help_(hub):
    hub.config.ARGS["parser"].print_help()
