# https://gitlab.eng.vmware.com/nvibert/sddc_import_export
# https://github.com/ga4gh/vrs-python


def __init__(hub):
    hub.pop.sub.add(dyne_name="idem")
    hub.pop.sub.load_subdirs(hub.vmc, recurse=True)


def cli(hub):
    hub.pop.config.load(["vmc", "idem", "acct", "rend"], cli="vmc")
    hub.pop.loop.create()
    hub.pop.Loop.run_until_complete(hub.vmc.init.start())


async def start(hub):
    hub.log.info("Logging into vmc client")
    ctx = await hub.idem.ex.ctx(
        path="exec.vmc.init",
        acct_file=hub.OPT.acct.acct_file,
        acct_key=hub.OPT.acct.acct_key,
        acct_profile=hub.OPT.idem.acct_profile,
    )

    result = None

    try:
        if hub.SUBPARSER == "import":
            raise NotImplementedError("Coming soon!")
        elif hub.SUBPARSER == "export":
            await hub.vmc.export.run(ctx)
        elif hub.SUBPARSER == "client":
            intent = hub.OPT.vmc.intent.lower()
            if intent.startswith("show-"):
                intent = intent[5:]
                result = await hub.vmc.client.show(ctx, intent)
            else:
                result = await hub.vmc.client.intent(ctx, intent)
        elif hub.SUBPARSER == "show":
            result = await hub.vmc.client.show(ctx, hub.OPT.vmc.intent.lower())

        outputter = hub.OPT.vmc.output
        if outputter is None:
            if isinstance(result, list):
                outputter = "table"
            else:
                outputter = "nested"

        format_key = lambda k: str(k).replace("_", " ").title()
        if isinstance(result, dict):
            result = {
                format_key(k): v
                for k, v in result.items()
                if not str(k).startswith("_")
            }
        if isinstance(result, list):
            result = [
                {format_key(k): v for k, v in r.items() if not str(k).startswith("_")}
                for r in result
            ]
        output: str = hub.output[outputter].display(result)
        print(output)
    except Exception as e:
        hub.log.error(f"vmctool {hub.SUBPARSER}: {e.__class__.__name__}: {e}")
        raise
    finally:
        await hub.acct.init.close()
