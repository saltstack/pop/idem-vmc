from typing import Awaitable
import aiofiles
import asyncio
import glob
import json
import os
import pathlib
import tempfile
from tqdm.asyncio import tqdm


async def write(hub, file_name: pathlib.Path, coro: Awaitable):
    """
    Asynchronously write json serializable data to a file
    """
    # Schedule and run the function that was passed
    hub.log.info(f"Scheduling retrieval for {file_name.stem}")
    data = await coro
    hub.log.info(f"Writing {file_name.stem} data to {file_name}")
    # Serialize the json data
    dump = json.dumps(data)
    # TODO sanitize the data to not have any keys with "password" or "key"
    async with aiofiles.open(file_name, "wb+") as fh_:
        await fh_.write(dump.encode())
    hub.log.info(f"Wrote {file_name.stem} data to {file_name}")
    return file_name


async def run(hub, ctx):
    org_id = hub.OPT.vmc.org_id or ctx.acct.default_org_id
    sddc_id = hub.OPT.vmc.sddc_id or ctx.acct.default_sddc_id

    # Setup output folder
    if hub.OPT.vmc.folder:
        output_dir = pathlib.Path(hub.OPT.vmc.folder)
    else:
        output_dir = pathlib.Path(tempfile.gettempdir(), "vmctool")

    if hub.OPT.vmc.clean:
        hub.log.warning("Deleting old JSON export files...")
        await asyncio.sleep(5)
        for file_name in glob.glob(str(output_dir / "*.json")):
            hub.log.warning(f"Deleting {file_name}")
            os.remove(file_name)

    # Make sure that the directories exist
    output_dir.mkdir(exist_ok=True)

    if "all" in hub.OPT.vmc.gather:
        gather = hub.vmc.export._funcs
    else:
        gather = hub.OPT.vmc.gather

    # Run all selected export functions
    coros = []
    for gatherer in gather:
        gatherer = gatherer.lower().replace("-", "_")
        if gatherer in ("write", "run"):
            continue
        hub.log.info(f"Getting data from {gatherer}")
        if gatherer in hub.vmc.export._funcs:
            func = getattr(hub.vmc.export, gatherer)(
                ctx, sddc_id=sddc_id, org_id=org_id
            )
            data_file = output_dir / f"{gatherer}.json"
            coro = hub.vmc.export.write(data_file, func)
            coros.append(coro)
        else:
            hub.log.error(f"There is no such function '{gatherer}'")

    # Show a progress bar for the functions being awaited
    progress = tqdm()
    for ret in progress.as_completed(coros, loop=hub.pop.Loop):
        path = await ret
        progress.set_description_str(f"Wrote {path}", refresh=True)
    progress.close()


async def sddc(hub, ctx, sddc_id, **kwargs):
    return await hub.exec.vmc.sddc.get(ctx, sddc_id=sddc_id)


async def service(hub, ctx, sddc_id, **kwargs):
    return await hub.tool.vmc.nsxt_proxy.get(
        ctx, "policy/api/v1/infra/services", sddc_id=sddc_id
    )


async def mgw_group(hub, ctx, sddc_id, **kwargs):
    return await hub.tool.vmc.nsxt_proxy.get(
        ctx, "policy/api/v1/infra/domains/mgw/groups", sddc_id=sddc_id
    )


async def mgw_rule(hub, ctx, sddc_id, **kwargs):
    return await hub.tool.vmc.nsxt_proxy.get(
        ctx,
        "policy/api/v1/infra/domains/mgw/gateway-policies/default/rules",
        sddc_id=sddc_id,
    )


async def cgw_group(hub, ctx, sddc_id, **kwargs):
    return await hub.tool.vmc.nsxt_proxy.get(
        ctx, "policy/api/v1/infra/domains/cgw/groups", sddc_id=sddc_id
    )


async def cgw_rule(hub, ctx, sddc_id, **kwargs):
    return await hub.tool.vmc.nsxt_proxy.get(
        ctx,
        "policy/api/v1/infra/domains/cgw/gateway-policies/default/rules",
        sddc_id=sddc_id,
    )


async def cgw_network(hub, ctx, sddc_id, **kwargs):
    return await hub.tool.vmc.nsxt_proxy.get(
        ctx, "policy/api/v1/infra/tier-1s/cgw/segments", sddc_id=sddc_id,
    )


async def dfw_network(hub, ctx, sddc_id, **kwargs):
    return await hub.tool.vmc.nsxt_proxy.get(
        ctx, "policy/api/v1/infra/domains/cgw/security-policies", sddc_id=sddc_id,
    )


async def dfw_network_details(hub, ctx, sddc_id, **kwargs):
    sddc_DFWrules = await hub.tool.vmc.nsxt_proxy.get(
        ctx, "policy/api/v1/infra/domains/cgw/security-policies", sddc_id=sddc_id,
    )
    cmap_ids = sorted(cmap["id"] for cmap in sddc_DFWrules)
    coros = []
    for cid in cmap_ids:
        hub.log.info(f"Scheduling retrieval for cmap {cid}")
        coro = hub.tool.vmc.nsxt_proxy.get(
            ctx,
            f"policy/api/v1/infra/domains/cgw/security-policies/{cid}/rules",
            sddc_id=sddc_id,
        )
        coros.append(coro)

    ret = {}
    for cid, result in zip(cmap_ids, await asyncio.gather(*coros, loop=hub.pop.Loop)):
        hub.log.info(f"Retrieved results for cmap {cid}")
        ret[cid] = result
    return ret


async def public_ip(hub, ctx, sddc_id, **kwargs):
    return await hub.tool.vmc.nsxt_proxy.get(
        ctx, "cloud-service/api/v1/infra/public-ips", sddc_id=sddc_id,
    )


async def nat_rule(hub, ctx, sddc_id, **kwargs):
    return await hub.tool.vmc.nsxt_proxy.get(
        ctx, "policy/api/v1/infra/tier-1s/cgw/nat/USER/nat-rules", sddc_id=sddc_id,
    )


async def service_access(hub, ctx, sddc_id, **kwargs):
    return await hub.tool.vmc.nsxt_proxy.get(
        ctx, "cloud-service/api/v1/infra/linked-vpcs", sddc_id=sddc_id,
    )


async def vpn_dpd_profile(hub, ctx, sddc_id, **kwargs):
    return await hub.tool.vmc.nsxt_proxy.get(
        ctx, "policy/api/v1/infra/ipsec-vpn-dpd-profiles", sddc_id=sddc_id,
    )


async def vpn_ike_profile(hub, ctx, sddc_id, **kwargs):
    return await hub.tool.vmc.nsxt_proxy.get(
        ctx, "policy/api/v1/infra/ipsec-vpn-ike-profiles", sddc_id=sddc_id,
    )


async def vpn_tunnel(hub, ctx, sddc_id, **kwargs):
    return await hub.tool.vmc.nsxt_proxy.get(
        ctx, "policy/api/v1/infra/ipsec-vpn-tunnel-profiles", sddc_id=sddc_id,
    )


async def vpn_bgp_neighbor(hub, ctx, sddc_id, **kwargs):
    return await hub.tool.vmc.nsxt_proxy.get(
        ctx,
        "policy/api/v1/infra/tier-0s/vmc/locale-services/default/l2vpn-services/default/sessions",
        sddc_id=sddc_id,
    )


async def vpn_l2_config(hub, ctx, sddc_id, **kwargs):
    return await hub.tool.vmc.nsxt_proxy.get(
        ctx,
        "policy/api/v1/infra/tier-0s/vmc/locale-services/default/l2vpn-services/default/sessions",
        sddc_id=sddc_id,
    )


async def vpn_l3_config(hub, ctx, sddc_id, **kwargs):
    return await hub.tool.vmc.nsxt_proxy.get(
        ctx,
        "policy/api/v1/infra/tier-0s/vmc/locale-services/default/ipsec-vpn-services/default/sessions",
        sddc_id=sddc_id,
    )
