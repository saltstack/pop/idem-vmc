import asyncio
from enum import Enum


class Status(Enum):
    CANCELED = "CANCELED"
    CANCELING = "CANCELING"
    FAILED = "FAILED"
    FINISHED = "FINISHED"
    STARTED = "STARTED"


async def wait(hub, ctx, task_id: str, interval: int = 10):
    while True:
        task = await hub.exec.vmc.org.task.get(ctx, task_id)
        if task.status == Status.FINISHED.value:
            hub.log.info(f"Task {task_id} finished successfully")
            return task
        elif task.status == Status.FAILED.value:
            hub.log.error(f"Task {task_id} failed")
            raise ChildProcessError(f"Task failed {task_id}")
        elif task.status == Status.CANCELED.value:
            hub.log.error(f"Task {task_id} cancelled")
            raise ChildProcessError(f"Task cancelled {task_id}")
        else:
            hub.log.debug(
                f"Estimated time remaining: {task.estimated_remaining_minutes} minutes"
            )
            await asyncio.sleep(interval)
