__func_alias__ = {"list_": "list"}


async def list_(hub, ctx, **kwargs):
    return await hub.tool.vmc.org.get(ctx, "payment-methods", **kwargs)
