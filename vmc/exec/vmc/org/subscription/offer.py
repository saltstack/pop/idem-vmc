__func_alias__ = {"list_": "list"}


async def list_(hub, ctx, region: str, product_type: str = "", **kwargs):
    return await hub.tool.vmc.org.get(
        ctx,
        "subscriptions/offer-instances",
        region=region,
        product_type=product_type,
        **kwargs,
    )
