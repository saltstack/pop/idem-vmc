from typing import List

__func_alias__ = {"list_": "list"}


async def list_(hub, ctx, sddcs: List[str] = None, states: List[str] = None):
    return await hub.tool.vmc.org.post(
        ctx, "tbrs/reservation", json={"sddcs": sddcs or [], "states": states or [],},
    )
