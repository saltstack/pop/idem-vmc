async def get(hub, ctx, terms_id: str = None):
    return await hub.tool.vmc.org.get(ctx, "tos", termsId=terms_id)
