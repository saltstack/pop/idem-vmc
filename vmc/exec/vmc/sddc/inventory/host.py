# https://developer.vmware.com/docs/vmc/latest/sddcs/esxs/


async def create(hub, ctx, sddc_name: str, **kwargs):
    return await hub.tool.vmc.org.post(
        ctx, f"sddcs/{sddc_name}/esxs", action="add", **kwargs
    )


async def delete(hub, ctx, sddc_name: str, **kwargs):
    return await hub.tool.vmc.org.post(
        ctx, f"sddcs/{sddc_name}/esxs", action="remove", **kwargs
    )
