__func_alias__ = {"list_": "list"}


async def create(hub, ctx, sddc_id: str, num_hosts: int, **kwargs):
    return await hub.tool.vmc.org.post(
        ctx, f"sddcs/{sddc_id}/clusters/", data=dict(num_nosts=num_hosts, **kwargs),
    )


async def config_constraints(hub, ctx, sddc_id, cluster_id: str):
    return await hub.tool.vmc.org.get(
        ctx, f"sddcs/{sddc_id}/clusters/{cluster_id}/config/constraints"
    )


async def storage_constraints(hub, ctx, num_hosts: int = 1, provider: str = "AWS"):
    return await hub.tool.vmc.org.get(
        ctx, "storage/cluster-constraints", num_hosts=num_hosts, provider=provider
    )


async def reconfigure(
    hub, ctx, sddc_id: str, cluster_id: str, num_hosts: int, **kwargs
):
    return await hub.tool.vmc.org.post(
        ctx,
        f"sddcs/{sddc_id}/clusters/{cluster_id}/reconfigure",
        data=dict(num_nosts=num_hosts, **kwargs),
    )


async def delete(hub, ctx, sddc_id: str, cluster_id: str):
    return await hub.tool.vmc.org.delete(ctx, f"sddcs/{sddc_id}/clusters/{cluster_id}")


async def set_msft_license(hub, ctx, sddc_id: str, cluster_id: str, **kwargs):
    return await hub.tool.vmc.org.post(
        ctx, f"sddcs/{sddc_id}/clusters/{cluster_id}/msft-licensing", data=kwargs
    )


async def primary(hub, ctx, sddc_name: str, **kwargs):
    return await hub.tool.vmc.org.get(
        ctx, f"sddcs/{sddc_name}/primarycluster", **kwargs
    )
