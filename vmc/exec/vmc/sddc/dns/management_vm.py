async def public(hub, ctx, sddc_id: str):
    """
    Update the DNS records of management VMs to use public IP addresses
    """
    await hub.tool.vmc.org.put(ctx, f"sddcs/{sddc_id}/dns/public")


async def private(hub, ctx, sddc_id: str):
    """
    Update the DNS records of management VMs to use private IP addresses
    """
    await hub.tool.vmc.org.put(ctx, f"sddcs/{sddc_id}/dns/public")


async def update(hub, ctx, sddc_id: str, management_vm_id: str, ip_type: str):
    await hub.tool.vmc.org.put(
        ctx, f"sddcs/{sddc_id}/management-vms/{management_vm_id}/dns/{ip_type}"
    )
