# https://developer.vmware.com/docs/vmc/latest/sddcs/sddc-templates/
__func_alias__ = {"list_": "list"}


async def delete(hub, ctx, template_id: str, **kwargs):
    return await hub.tool.vmc.org.delete(ctx, f"sddc-templates/{template_id}", **kwargs)


async def get(hub, ctx, template_id: str = None, sddc_id: str = None, **kwargs):
    if template_id:
        return await hub.tool.vmc.org.get(
            ctx, f"sddc-templates/{template_id}", **kwargs
        )
    elif sddc_id:
        return await hub.tool.vmc.org.get(
            ctx, f"sddcs/{sddc_id}/sddc-template", **kwargs
        )
    else:
        raise SyntaxError("Missing arg template_id or sddc_id")


async def list_(hub, ctx, **kwargs):
    return await hub.tool.vmc.org.get(ctx, f"sddc-templates", **kwargs)
