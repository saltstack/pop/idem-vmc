import pytest


@pytest.mark.skipif(True, reason="")
@pytest.mark.asyncio
async def test_create(hub, ctx, instance_name):
    ret = await hub.exec.vmc.sddc.create(
        ctx,
        name=instance_name,
        region="us-west-1",
        deployment_type="SingleAZ",
        num_hosts=1,
    )
    assert ret


@pytest.mark.skipif(True, reason="")
@pytest.mark.asyncio
async def test_list(hub, ctx, instance_name):
    ret = await hub.exec.vmc.sddc.list(ctx)
    assert any(instance_name == sddc["name"] for sddc in ret)


@pytest.mark.skipif(True, reason="")
@pytest.mark.asyncio
async def test_get(hub, ctx, instance_name):
    sddc_id = await hub.exec.vmc.sddc.id(ctx, instance_name)
    ret = await hub.exec.vmc.sddc.get(ctx, sddc_id)
    assert ret["name"] == instance_name
    assert ret["id"] == sddc_id


@pytest.mark.skipif(True, reason="")
@pytest.mark.asyncio
async def test_delete(hub, ctx, instance_name):
    ret = await hub.exec.vmc.sddc.delete(ctx, instance_name)
    assert ret, ret
